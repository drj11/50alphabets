[brutal]: #compile "yes"

<style>
img {
  width: 100%;
}
</style>

# Now i am 50, 50 alphabets

Before my 51st birthday, make 50 alphabets.

## 0 [Bad 8](https://gitlab.com/drj11/font-bad8/)

![An alphabet of pixels](00bad8.png)

## 1 Small (inferior) numbers for [Airvent](https://gitlab.com/drj11/font-airvent)

![Numbers, big and small, and fraction one-half](01airvent-smol.png)

## 1½ Numbers for [Antscap](https://gitlab.com/drj11/font-antscap)

![Numbers, Antscap](015antscap-number.png)

## 2 Alphabet for [Abeam](https://gitlab.com/drj11/font-abeam)

![Alphabet, Antscap](02abeam.png)

## 3 Alphabet for [Adone](https://gitlab.com/drj11/font-adone)

A hand-and-mouse trace of a Walter Tracy design.

![Alphabet, Adone](03adone.png)

## 4 Alphabet for [Avend](https://gitlab.com/drj11/font-avend)

A digital interpretation, using a hand-lens, of Devendra.

![Alphabet, Avend](04avend.svg)

## 5 Alphabet for [Necker](https://gitlab.com/drj11/font-necker)

![Alphabet, Necker](05necker.png)

## 6 Lower case alphabet for [Necker](https://gitlab.com/drj11/font-necker) (above)

![Lower Case Alphabet, Necker](06necker-lc.png)

## 7 Alphabet for [Asmol](https://gitlab.com/drj11/font-asmole)

![Alphabet, Asmol](07asmol.svg)

Designed on a 10 point grid.
I wrote a [blog article about designing on the grid](https://home.octetfont.com/blog/10points.html).

## 8 Alphabet for [Arugula](https://gitlab.com/drj11/font-arugula)

![Alphabet, Arugula](08arugula.svg)

Traced from a photograph of stencilled letters.

## 9 [Pellucida](https://gitlab.com/drj11/pellucida)

This is in fact an entirely automatic trace of the bitmap font
Pellucida from Bigelow & Holmes licensed in the 1980s and 1990s.

I'm sneaking it in here because it was considerable work to
track it down and write the tools to trace it.

![Alphabet upper and lower case, Pellucida 8](09pelm.8.svg)

## 10 Mono Alphabet for [Arugula](https://gitlab.com/drj11/font-arugula)

![Monospaced Alphabet, Arugula](10ArugulaMono.svg)

A monospaced alphabet, based on Arugula (some letters more so
than others).

## 11 Little letters for [Bad 8](https://gitlab.com/drj11/font-bad8/)

A reprise of the alphabet that kicked all this off.
I draw the little letters / lower case.

![Monospaced Pixel Alphabet, Bad8](11Bad8-lower.svg)


## 12 Capital letters for [Dot 5](https://fontstruct.com/fontstructions/show/2151461/dot5)

![Dot5](12dot5-upper.svg)


## 13 An Exercise in [fraktur](https://gitlab.com/drj11/font-ax1frak)

The second font, after [Ranalina](https://gitlab.com/drj11/ranalina/),
to be made with my own [Font 8 tools](https://git.sr.ht/~drj/font8).

![ax1frak](13ax1frak.svg)

## 14 Modular alphabet [Arfboxx](https://gitlab.com/drj11/font-arfboxx)

Also made with Font 8.

![Arfboxx](14arfboxx.svg)


## 15 A cheeky remix [Augeron](https://gitlab.com/drj11/font-augeron)

Serifs modified to rings (on a not-for-redistribution font)

![Augeron](15augeron.svg)

## 16 (and a ½) a pen sketch: [Aromech](https://gitlab.com/drj11/font-aromech)

A pen sketch digitised.

![aromech](16aromech.svg)

I cleaned it up, in a version called Aromelt, and added numbers;
and bonus astronomical bodies.

![numbers and bonus planets](16½aromelt.svg)

## 17 just numbers and a name with no charm [A290](https://gitlab.com/drj11/font-a290)

![numbers](17a290.svg)

## 18 (and a ½) Using the ZX81 Semigraphics [ZXShade](https://gitlab.com/drj11/font-zxshade)

![blocky zxshade](18zxshade.svg)

I forgot i made numbers (until i came to review the font for
this post). For an additional ½ point.

![zxshade numbers](18½zxshade.svg)

## 19½ and 20 and 21 the capstone for 2022, [Arhinni](https://drj11.gitlab.io/microsite-arhinni/arhinni.html)

I signed up for a course with ILT and this font is the major
part of that.

Most of the fonts shown here are more like sketches, rapidly
drawn or made and digitised, with almost no going over.
Arhinni is more filled out in repertoire and most of it has been
reviewed and reworked.

Here i'm counting it for 2½ though it probably deserves more:
it has > 30 letters in its base alphabet, in upper and and lower
case; it has about half an alphabet of alts; lining and ranging
numbers; about an alphabet of astronomical and alchemical signs;
and most of the accents used in Latin scripts.

You can explore [Arhinni on its microsite](https://drj11.gitlab.io/microsite-arhinni/arhinni.html)

![Arhinni lower](19arhinni.svg)

![Arhinni upper](20arhinni.svg)

![Arhinni lining numbers](20½arhinni.svg)

## 22 Capitals for [Artzoid](https://gitlab.com/drj11/font-artzoid/)

Sketched on paper on 2021-08-21, i had digitised the lower case
ages ago, but the capital only recently.
By mouse-and-eye rather than tracing.

![Artzoid](22artzoid.svg)

## 23 Lower case for [F012](https://gitlab.com/drj11/font-012-flare)

A new digital drawing mostly inspired by a
reworking of the **Z** from previous.

![F012](23f012.svg)


## 24 Pencil sketch [Adjuvant](https://gitlab.com/drj11/font-adjuvant)

Magical pencil stroke. Shame about my amateur lines.

![Adjuvant](24adjuvant.svg)


## The rules

- A to Z in _either_ case counts for 1;
- 0 to 9 counts for ½;
- OTF or GTFO, sketches don't count
  (but lino / vinyl / clay would be okay);
- completing existing projects count, but don't cheat;
- revivals / rip-offs okay;
- spacing not required;
- non-latin scripts use best judgement (for example, a new
  Cyrillic or Greek is probably 1, but adding a cyrillic maybe
  only ½).

# END
